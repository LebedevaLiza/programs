#ifndef STRUCTDATA_H
#define STRUCTDATA_H

#include <QByteArray>
#include <QDataStream>
#include <QString>
#include <QDateTime>

struct StructData {
    QDateTime timestamp;
    QString operation;
    int value;
    QVector<int> recordStack;

public:
    QDateTime getTimestamp() const { return timestamp; }
    void setTimestamp(const QDateTime& value) { timestamp = value; }

    QString getOperation() const { return operation; }
    void setOperation(const QString& value) { operation = value; }

    int getValue() const { return value; }
    void setValue(int newValue) { value = newValue; }

    QVector<int> getRecordStackr() const { return recordStack; }
    void setRecordStack(const QVector<int>& newRecordStack) { recordStack = newRecordStack; }


    QByteArray toByteArray() const {
        QByteArray data;
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream << timestamp << operation << value << recordStack;
        return data;
    }

    static StructData fromByteArray(const QByteArray &data) {
        StructData result;
        QDataStream stream(data);
        stream >> result.timestamp >> result.operation >> result.value >> result.recordStack;
        return result;
    }
};

#endif // STRUCTDATA_H
