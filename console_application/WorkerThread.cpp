#include "WorkerThread.h"
#include "QCoreApplication"


WorkerThread::WorkerThread(QObject *parent)
    : QThread(parent), numElements(0), shouldStop(false), startElement(0){}

void WorkerThread::setData(int numElements, int startIndex, QVector<int>* stack) {
    QMutexLocker locker(&mutex);
    this->numElements = numElements;
    this->startElement = startIndex;
    this->stack = stack;
}

int WorkerThread::getStartElement(){
   return startElement;
}

void WorkerThread::stop()
{
    QMutexLocker locker(&mutex);
    shouldStop = true;
}

void WorkerThread::read()
{
    if(stack->size() >= numElements){
        qDebug() << "Чтение стека";
            for (auto it = stack->rbegin(); it != stack->rend(); ++it) {
                if (shouldStop) {
                    startElement = -1;
                    emit stopped();
                    return;
                }
            qDebug() << *it;
            QThread::msleep(100);
        }
    }
}

void WorkerThread::run() {
    if (startElement != -1){
    for (int i = startElement; i < numElements; ++i) {
        {
            QMutexLocker locker(&mutex);
            if (shouldStop) {
                emit stopped();
                startElement = i;
                return;
            }
            stack->push_back(i);
            emit stackChanged(*stack);
        }
        QThread::msleep(100);
        read();
    }
    }else read();

    emit stopped();


}
