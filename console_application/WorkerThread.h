// Новый класс WorkerThread.h
#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QThread>
#include <QDebug>
#include <QMutex>


class WorkerThread : public QThread
{
    Q_OBJECT

public:
    explicit WorkerThread(QObject *parent = nullptr);
    void setData(int numElements, int startIndex, QVector<int>* stack);
    void stop();
    void read();
    int getStartElement();

    void pushToStack(int value);
    int popFromStack();
    const QVector<int>& getStack() const;

signals:
    void stopped();
    void stackChanged(const QVector<int>& stack);

protected:
    void run() override;

private:
    int numElements;
    bool shouldStop;
    int startElement;
    QMutex mutex;
    QVector<int>* stack;

};


#endif // WORKERTHREAD_H
