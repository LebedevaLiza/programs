#ifndef CONSOLE_H
#define CONSOLE_H

#include"QUdpSocket"
#include<../structData.h>
#include <QByteArray>
#include <QDebug>
#include<WorkerThread.h>


class Console : public QObject
{
    Q_OBJECT
public:
    Console();
    ~Console();
    int startIndex;
    QVector<int> stack;


public slots:
    void ReadingData();

private:
    QUdpSocket *socket;
    WorkerThread* workerThread;
    void onWorkerThreadFinished();
    int numElements;
};

#endif // CONSOLE_H
