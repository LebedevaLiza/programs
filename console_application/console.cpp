#include "console.h"


Console::Console()
{
    startIndex = 0;
    socket = new QUdpSocket(this);
    socket->bind(QHostAddress::LocalHost, 1111);
    connect(socket, &QUdpSocket::readyRead, this, &Console::ReadingData);
    workerThread = nullptr;

}
void Console::ReadingData() {
    QHostAddress sender;
    quint16 senderPort;
    while(socket->hasPendingDatagrams()){

        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        socket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

        StructData receivedData = StructData::fromByteArray(datagram);

        if (!receivedData.operation.isEmpty()){
            numElements = 0;

            numElements = receivedData.value;
            if (receivedData.operation == "Start") {
                qDebug() << receivedData.operation << " at" << receivedData.timestamp.toString();

                StructData recordData;
                recordData.setTimestamp(QDateTime::currentDateTime());
                recordData.setOperation("Record");
                recordData.setValue(numElements);
                qDebug() << recordData.operation << " at" << recordData.timestamp.toString();

                socket->writeDatagram(recordData.toByteArray(), QHostAddress::LocalHost, 1112);


                if (workerThread) {
                    workerThread->stop();
                    workerThread->wait();
                }
                    workerThread = new WorkerThread();
                    connect(workerThread, &QThread::finished, this, &Console::onWorkerThreadFinished);
                    connect(workerThread, &WorkerThread::finished, workerThread, &QObject::deleteLater);

                workerThread->setData(numElements, startIndex, &stack);
                workerThread->start();

            }  else if (receivedData.operation == "Stop") {
                qDebug() << receivedData.operation << " at" << receivedData.timestamp.toString();
                if (workerThread) {
                    workerThread->stop();
                    workerThread->wait();
                    startIndex = workerThread->getStartElement();
                    delete workerThread;
                    workerThread = nullptr;
                }
            }
        } else {
            QString receivedMessage = QString(datagram);
            qDebug() << receivedMessage;
        }
    }
}

void Console::onWorkerThreadFinished() {
    qDebug() << stack;
    if (stack.size() == numElements && numElements!=0) {
        StructData recordData;
        recordData.setTimestamp(QDateTime::currentDateTime());
        recordData.setOperation("Record stack finished");
        recordData.setValue(numElements);
        recordData.setRecordStack(stack);

        stack.clear();
        qDebug() << recordData.operation << " at" << recordData.timestamp.toString() << " Recording elements " << recordData.getRecordStackr();
        socket->writeDatagram(recordData.toByteArray(), QHostAddress::LocalHost, 1112);

    }
    if (workerThread) {
        startIndex = 0;
        delete workerThread;
        workerThread = nullptr;
    }

    socket->writeDatagram("Worker thread stopped", QHostAddress::LocalHost, 2222);
}


Console::~Console()
{
    delete socket;
}
