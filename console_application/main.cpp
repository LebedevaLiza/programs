#include <QCoreApplication>
#include <QThread>
#include <console.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Console console;

    // Создаем поток и перемещаем в него объект console
    QThread consoleThread;
    console.moveToThread(&consoleThread);

    QObject::connect(&a, &QCoreApplication::aboutToQuit, &consoleThread, &QThread::quit);

    // Запускаем поток
    consoleThread.start();

    // Завершаем программу по завершении потока
    QObject::connect(&consoleThread, &QThread::finished, &a, &QCoreApplication::quit);

    qRegisterMetaType<QVector<int>>("QVector<int>");

    return a.exec();
}
