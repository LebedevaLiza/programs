QT -= gui
QT += network

CONFIG += c++17 console
CONFIG -= app_bundle

SOURCES += \
        WorkerThread.cpp \
        console.cpp \
        main.cpp

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    WorkerThread.h \
    console.h
