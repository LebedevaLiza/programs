#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    start = false;
    startButton = new QPushButton("Старт", this);
    stopButton = new QPushButton("Стоп", this);

    inputLineEdit = new QLineEdit(this);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(startButton);
    layout->addWidget(stopButton);
    layout->addWidget(inputLineEdit);

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(layout);

    setCentralWidget(centralWidget);

    connect(startButton, &QPushButton::clicked, this, &MainWindow::startClicked);
    connect(stopButton, &QPushButton::clicked, this, &MainWindow::stopClicked);

    socket = new QUdpSocket(this);
    socket->bind(QHostAddress::LocalHost, 2222);
    connect(socket, &QUdpSocket::readyRead, this, &MainWindow::ReadingData);
}

MainWindow::~MainWindow()
{
    delete startButton;
    delete stopButton;
    delete inputLineEdit;
}
void MainWindow::ReadingData(){
    QHostAddress sender;
    quint16 senderPort;

    while(socket->hasPendingDatagrams()){
        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        socket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);
        if (QString(datagram) == "Worker thread stopped"){
            start = false;
            inputLineEdit->setEnabled(true);
        }
    }
}

void MainWindow::startClicked()
{
    if (!start) {
        StructData data;
        bool conversionOK;
        int numElements = inputLineEdit->text().toInt(&conversionOK);

        if (conversionOK) {
            start = true;

            data.timestamp = QDateTime::currentDateTime();
            data.operation = "Start";
            data.value = numElements;

            socket->writeDatagram(data.toByteArray(), QHostAddress::LocalHost, 1111);
            socket->writeDatagram(data.toByteArray(), QHostAddress::LocalHost, 1112);
            inputLineEdit->setEnabled(false);


        } else {
            socket->writeDatagram("Необходимо ввести целое число", QHostAddress::LocalHost, 1111);
        }
    } else {
        socket->writeDatagram("Кнопка старт уже нажата", QHostAddress::LocalHost, 1111);
    }
}
void MainWindow::stopClicked()
{
    if (start) {
        start = false;

        StructData data;
        data.timestamp = QDateTime::currentDateTime();
        data.operation = "Stop";
        data.value = -1;

        socket->writeDatagram(data.toByteArray(), QHostAddress::LocalHost, 1111);
        socket->writeDatagram(data.toByteArray(), QHostAddress::LocalHost, 1112);
        inputLineEdit->setEnabled(false);

    } else {
        socket->writeDatagram("Кнопка стоп уже нажата", QHostAddress::LocalHost, 1111);
    }
}

