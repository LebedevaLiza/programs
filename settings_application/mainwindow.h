#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>
#include"QUdpSocket"
#include <QDateTime>
#include<../structData.h>


class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    QTimer *timer;
    QPushButton *startButton;
    QPushButton *stopButton;
    QLineEdit *inputLineEdit;
    QUdpSocket *socket;
    bool start;

public slots:
    void ReadingData();

private slots:
    void startClicked();
    void stopClicked();

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
};
#endif // MAINWINDOW_H
