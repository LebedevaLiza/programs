    #include "resultwindow.h"

ResultWindow::ResultWindow(QWidget *parent)
    : QMainWindow(parent)
{
    socket = new QUdpSocket(this);
    socket->bind(QHostAddress::LocalHost, 1112);
    model = new QStringListModel(this);

    listView = new QListView(this);
    listView->setModel(model);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(listView);

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(layout);
    setCentralWidget(centralWidget);

    resize(400, 300);
    setWindowTitle("Output");

    connect(socket, &QUdpSocket::readyRead, this, &ResultWindow::addListItem);
}

ResultWindow::~ResultWindow()
{
    delete model;
    delete listView;
}

void ResultWindow::addListItem()
{
    QHostAddress sender;
    quint16 senderPort;

    while(socket->hasPendingDatagrams()){
        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        socket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

        StructData receivedData = StructData::fromByteArray(datagram);
        QString message;
        if (receivedData.value > 0) {
            QVector<int> stack = receivedData.getRecordStackr();

            if(!stack.isEmpty()){
                QString stackString;
                QTextStream stream(&stackString);

                for (int value : stack) {
                    stream << value << ' ';
                }
            message = receivedData.operation + " at " + receivedData.timestamp.toString() + " " + QString::number(receivedData.value) + " elements: " + stackString;
            } else {
            message = receivedData.operation + " at " + receivedData.timestamp.toString() + " " + QString::number(receivedData.value) + " elements";
            }
        } else message = receivedData.operation + " at " + receivedData.timestamp.toString();
        datagram.resize(socket->pendingDatagramSize());
        socket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);
        model->insertRow(model->rowCount());
        QModelIndex index = model->index(model->rowCount() - 1);
        model->setData(index, message);

        listView->scrollTo(index);
    }
}



