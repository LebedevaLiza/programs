#ifndef RESULTWINDOW_H
#define RESULTWINDOW_H

#include <QMainWindow>
#include <QListView>
#include <QStringListModel>
#include <QVBoxLayout>
#include<QUdpSocket>
#include<../structData.h>

class ResultWindow : public QMainWindow
{
    Q_OBJECT

public:
    ResultWindow(QWidget *parent = nullptr);
    ~ResultWindow();
    void addListItem();

private:
    QStringListModel *model;
    QListView *listView;
    QUdpSocket *socket;
};
#endif // RESULTWINDOW_H
